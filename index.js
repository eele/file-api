var express = require('express');
var cors = require('cors');
var app = express();
var formidable = require('formidable');
var fs = require('fs');
var mv = require('mv');
var path = require('path');
var crypto = require('crypto');
var multer  = require('multer');

const UPLOAD_DIR = 'uploads/';
const TMP_UPLOAD_DIR = 'tmp/'
var upload = multer({ dest: TMP_UPLOAD_DIR });

console.log('Started file upload server');

app.use(cors());
console.log('CORS enabled for all origins (*)');

// accept data
app.post('/', upload.single('data'), function(req, res) {
  // `data` is the name of the <input> field of type `file`
  console.log('Uploading file: ', req.file);
  var old_path = req.file.path;
  var file_size = req.file.size;
  var file_ext = req.file.originalname.split('.').pop();
  var index = old_path.lastIndexOf(path.sep) + 1;
  var file_name = old_path.substr(index);
  var file_secret = crypto.randomBytes(22).toString('hex') + '.' + file_ext;
  var file_url = req.protocol + '://' + req.get('host') + '/' + file_secret;

  var new_path = path.join(__dirname, UPLOAD_DIR, file_secret);

  mv(old_path, new_path, (err) => {
    if (err) {
      console.log('Error: ', err);
      res.status(500);
      res.json({'success': false});
    } else {
      console.log('Moved to: ', new_path);
      res.status(200);
      res.json({
            secret: file_secret,
            name: file_name,
            size: file_size,
            url: file_url,
            contentType: file_ext,
          });
    }
  });
});

app.get('/:fileId', function(req, res) {
  var fileId = req.params.fileId;
  var file = path.join(__dirname, UPLOAD_DIR, fileId);
  res.sendFile(file);
});

app.get('/download/:fileId', function(req, res) {
  var fileId = req.params.fileId;
  var fileName = req.query.name;
  var file = path.join(__dirname, UPLOAD_DIR, fileId);
  res.download(file, fileName);
});

app.listen(3000);
